package integration;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import ab.common.PlayerModel;
import org.junit.*;

import ab.*;
import ab.playlist.*;

public class Playertest {

    TestableMediaPlayer mediaPlayer;

    Playlist playlist;

    PlayerModel player;

    @Before
    public void init() {
        playlist = new Playlist();
        playlist.addDir(
                new TestableDir("/files")
                    .addFile("b.mp3")
                    .addFile("a.mp3"));

        mediaPlayer = new TestableMediaPlayer();

        PlaylistLoader playlistLoader = new PlaylistLoader(null) {
            @Override
            public Playlist load() {
                return playlist;
            }
        };

        player = new PlayerModel(playlistLoader, mediaPlayer, null);
    }

    @Test
    public void startsFromSavedTime() {
        playlist.setCurrentTime(5);

        player.play();

        assertThat(mediaPlayer.isPlaying(), is(true));
        assertThat(mediaPlayer.getCurrent().getName(), is("a.mp3"));
        assertThat(mediaPlayer.getCurrentTime(), is(5));
    }

    @Test
    public void selectsNextWhenFileWhenFileEnds() {
        player.play();

        assertThat(mediaPlayer.isPlaying(), is(true));
        assertThat(mediaPlayer.getCurrent().getName(), is("a.mp3"));

        mediaPlayer.trackEnds();

        assertThat(mediaPlayer.isPlaying(), is(true));
        assertThat(mediaPlayer.getCurrent().getName(), is("b.mp3"));

        mediaPlayer.trackEnds();

        assertThat(mediaPlayer.isPlaying(), is(false));
    }

    @Test
    public void reloadsPlaylistOnDirChange() {

        playlist.setCurrentTime(5);
        player.play();

        PlaylistLoader playlistLoader = new PlaylistLoader(null) {
            @Override
            public Playlist load() {
                Playlist playlist = new Playlist();

                playlist.addDir(
                        new TestableDir("/files").addFile("a.mp3")
                                                 .addFile("c.mp3"));

                playlist.setCurrentTime(10);

                return playlist;
            }
        };

        player.reloadPlaylist(playlistLoader);

        assertThat(mediaPlayer.calls, contains(
                "load: a.mp3", "seek_5",
                "play", "stop",
                "load: a.mp3", "seek_10", "play"));
    }

}
