package integration;

import ab.fx.FxMediaPlayer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TestableMediaPlayer extends FxMediaPlayer {

    private File current;
    private Integer position;
    private boolean playing = false;
    Runnable onEndOfMedia;

    List<String> calls = new ArrayList<String>();

    @Override
    public Integer getCurrentTime() {
        calls.add("getCurrentTime");
        return position;
    }

    @Override
    public void stop() {
        calls.add("stop");
        playing = false;
    }

    public File getCurrent() {
        return current;
    }

    @Override
    public void load(File file, Runnable onReady) {
        calls.add("load: " + file.getName());
        this.current = file;
        onReady.run();
    }

    @Override
    public void setOnEndOfMedia(Runnable runnable) {
        this.onEndOfMedia = runnable;
    }

    @Override
    public boolean isPlaying() {
        return playing;
    }

    @Override
    public void play() {
        calls.add("play");
        playing = true;
    }

    @Override
    public void seek(Integer position) {
        calls.add("seek_" + position);
        this.position = position;
    }

    public void trackEnds() {
        playing = false;
        onEndOfMedia.run();
    }
}
