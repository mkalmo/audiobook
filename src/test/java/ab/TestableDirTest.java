package ab;

import org.junit.Test;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestableDirTest {

    @Test
    public void supportsSubDirs() {
        TestableDir root = new TestableDir("/root")
            .addFile("sub_1/a.mp3")
            .addFile("sub_2/c.mp3")
            .addFile("sub_1/b.mp3");

        File sub1 = root.listFiles()[0];
        File sub2 = root.listFiles()[1];

        assertThat(sub1.getName(), is("sub_1"));
        assertThat(sub2.getName(), is("sub_2"));

        assertThat(sub1.listFiles()[0].getName(), is("a.mp3"));
        assertThat(sub1.listFiles()[1].getName(), is("b.mp3"));

        assertThat(sub2.listFiles()[0].getName(), is("c.mp3"));
    }

}
