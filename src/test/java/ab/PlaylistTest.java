package ab;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.io.File;

import org.junit.Test;
import ab.playlist.Playlist;

public class PlaylistTest {

    @Test
    public void newPlaylistHasNoCurrentFile() {
        assertThat(new Playlist().hasCurrentFile(), is(false));
    }

    @Test
    public void addingDirAddsSortedFilesToPlaylist() {
        Playlist playlist = new Playlist();
        playlist.addDir(
                new TestableDir("/files")
                    .addFile("b.mp3")
                    .addFile("a.mp3"));

        assertThat(playlist.getCurrentFileName(), is("a.mp3"));

        playlist.next();

        assertThat(playlist.getCurrentFileName(), is("b.mp3"));
    }

    @Test
    public void returnsAllDirsInPlaylist() {
        Playlist playlist = getPlaylistWithTwoDirs();

        assertThat(playlist.getDirNames(), contains("files1", "files2"));
    }

    @Test
    public void returnsAllFilesCurrentDir() {
        Playlist playlist = getPlaylistWithTwoDirs();

        assertThat(playlist.getFileNames(), contains("c.mp3", "d.mp3"));
    }

    @Test
    public void addingSameDirDoesNotCauseDuplicateEntry() {
        Playlist playlist = new Playlist();
        playlist.addDir(new TestableDir("/files").addFile("a.mp3"));
        playlist.addDir(new TestableDir("/files").addFile("b.mp3"));

        assertThat(playlist.getDirNames(), contains("files"));
    }

    @Test
    public void emptyDirCanBeAdded() {
        Playlist playlist = new Playlist();
        playlist.addDir(getEmptyDir());
    }

    public void getCurrentFileWhenNoCurrentFileReturnsNull() {
        assertThat(new Playlist().getCurrentFileName(), is(nullValue()));
    }

    @Test
    public void selectingDirAcitvatesTheDir() {
        Playlist playlist = getPlaylistWithTwoDirs();

        assertThat(playlist.getCurrentFileName(), is("c.mp3"));

        playlist.selectDir("files1");

        assertThat(playlist.getCurrentFileName(), is("a.mp3"));
    }

    @Test
    public void playlistRemembersPositionInEachDir() {
        Playlist playlist = getPlaylistWithTwoDirs();
        playlist.selectDir("files1");
        playlist.next();

        assertThat(playlist.getCurrentFileName(), is("b.mp3"));

        playlist.selectDir("files2");

        assertThat(playlist.getCurrentFileName(), is("c.mp3"));

        playlist.selectDir("files1");

        assertThat(playlist.getCurrentFileName(), is("b.mp3"));
    }

    @Test
    public void filesCanBeSelected() {
        Playlist playlist = getPlaylistWithTwoDirs();
        playlist.selectDir("files1");
        playlist.selectFile("b.mp3");

        assertThat(playlist.getCurrentFileName(), is("b.mp3"));
    }

    @Test
    public void selectingNonExistentFileDeactivatesCurrent() {
        Playlist playlist = getPlaylistWithTwoDirs();
        playlist.selectDir("files1");
        playlist.selectFile("nonexistent");

        assertThat(playlist.hasCurrentFile(), is(false));
    }

    @Test
    public void positionCanBeAddedToCurrentFile() {
        Playlist playlist = getPlaylistWithTwoDirs();
        playlist.selectDir("files1");
        playlist.setCurrentTime(1);
        assertThat(playlist.getCurrentTime(), is(1));
    }

    @Test
    public void playlistStateCanBeSerialized() {
        Playlist playlist = getPlaylistWithTwoDirs();
        playlist.selectDir("files2");
        playlist.selectFile("d.mp3");
        playlist.setCurrentTime(1);

        String expected = "0|files1|a.mp3|0\n" +
                          "1|files2|d.mp3|1\n";

        assertThat(playlist.serializeState(), is(expected));
    }

    @Test
    public void emptyDirsCanBeSerialized() {
        Playlist playlist = new Playlist();
        playlist.addDir(new TestableDir("/files"));

        assertThat(playlist.serializeState(), is(""));
    }

    @Test
    public void singleFilesCanBeSerialized() {
        Playlist playlist = new Playlist();
        playlist.addFile(new File("a.mp3"));
        playlist.setCurrentTime(5);

        assertThat(playlist.serializeState(), is("1|a.mp3|a.mp3|5\n"));
    }

    @Test
    public void sigleFilesCanBeAdded() {
        Playlist playlist = new Playlist();
        playlist.addFile(new File("/files/x.mp3"));
        playlist.addFile(new File("/files/y.mp3"));

        assertThat(playlist.getDirNames(), contains("x.mp3", "y.mp3"));
        assertThat(playlist.getFileNames(), contains("y.mp3"));
    }

    @Test
    public void dirsAndSingleFilesCanBeMixed() {
        Playlist playlist = getPlaylistWithTwoDirs();
        playlist.addFile(new File("/files/x.mp3"));
        playlist.addFile(new File("/files/y.mp3"));

        playlist.selectDir("files1");

        assertThat(playlist.getCurrentDir(), is("files1"));
        assertThat(playlist.getCurrentFileName(), is("a.mp3"));

        playlist.selectDir("x.mp3");

        assertThat(playlist.getCurrentDir(), is("x.mp3"));
        assertThat(playlist.getCurrentFileName(), is("x.mp3"));
    }

    @Test
    public void reloadingDirPicksUpChanges() {
        Playlist playlist = new Playlist();
        TestableDir dir = new TestableDir("/files")
            .addFile("a.mp3");

        playlist.addDir(dir);

//        System.out.println(playlist.getCurrentFile().getPath());

//        playlist.reload("/files");
    }

    private TestableDir getEmptyDir() {
        return new TestableDir("/files");
    }

    private Playlist getPlaylistWithTwoDirs() {
        Playlist playlist = new Playlist();

        TestableDir book1 = new TestableDir("/files1")
            .addFile("a.mp3")
            .addFile("b.mp3");
        TestableDir book2 = new TestableDir("/files2")
            .addFile("c.mp3")
            .addFile("d.mp3");

        playlist.addDir(book1);
        playlist.addDir(book2);
        return playlist;
    }
}
