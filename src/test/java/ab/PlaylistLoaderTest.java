package ab;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import ab.playlist.Playlist;
import ab.playlist.PlaylistLoader;
import org.junit.Test;


public class PlaylistLoaderTest {

    TestableDir audioDir = new TestableDir("/books")
        .addFile("book1/a.mp3")
        .addFile("book2/b.mp3");

    @Test
    public void loadingAudioDirAddsAllSubdirs() {
        PlaylistLoader loader = new PlaylistLoader(audioDir);

        Playlist playlist = loader.load();

        assertThat(playlist.getDirNames(), contains("book1", "book2"));
    }

    @Test
    public void loaderLoadsSingleFilesToo() {
        PlaylistLoader loader = new PlaylistLoader(audioDir);

        audioDir.addFile("c.mp3");

        Playlist playlist = loader.load();

        assertThat(playlist.getDirNames(),
                contains("book1", "book2", "c.mp3"));
    }

    @Test
    public void infoFromSavedPlaylistIsAdded() {
        PlaylistLoader loader = new PlaylistLoader(audioDir) {
            @Override
            protected String loadPlaylistFile() {
                return "1|book1|a.mp3|15\n";
            }
        };

        Playlist playlist = loader.load();

        assertThat(playlist.getCurrentFileName(), is("a.mp3"));
        assertThat(playlist.getCurrentTime(), is(15));
    }

    @Test
    public void staleInfoFromSavedPlaylistIsIgnored() {
        PlaylistLoader loader = new PlaylistLoader(audioDir) {
            @Override
            protected String loadPlaylistFile() {
                return "0|nonexistent||0\n"
                     + "0|book1|nonexistent|0\n";
            }
        };

        loader.load();
    }

}
