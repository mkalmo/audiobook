package ab;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestableDir extends File {

    private static final long serialVersionUID = 1L;

    private List<File> files = new ArrayList<File>();

    public TestableDir(String path) {
        super(path);
    }

    public TestableDir(File path, String name) {
        super(path, name);
    }

    @Override
    public boolean isDirectory() {
        return true;
    }

    @Override
    public File[] listFiles() {
        return files.toArray(new File[0]);
    }

    @Override
    public File[] listFiles(FilenameFilter filter) {
        files.stream().filter(f -> {
            String filePath = f.getAbsolutePath().
                    substring(0, f.getAbsolutePath()
                            .lastIndexOf(File.separator));
            File path = new File(filePath);
            return filter.accept(path, f.getName());
        });

        return files.toArray(new File[0]);
    }

    private TestableDir getOrCreateSubDir(String subdir) {
        for (File file : files) {
            if (file.getName().equals(subdir)) {
                return (TestableDir) file;
            }
        }

        TestableDir newDir = new TestableDir(this, subdir);

        files.add(newDir);

        return newDir;
    }

    public TestableDir addFile(String filePath) {
        List<String> elements = new ArrayList<String>();
        elements.addAll(Arrays.asList(filePath.split("/")));
        int lastIndex = elements.size() - 1;
        String fileName = elements.get(lastIndex);
        elements.remove(lastIndex);

        TestableDir sub = createSubDirs(this, elements);

        sub.files.add(new File(sub, fileName));

        return this;
    }

    private TestableDir createSubDirs(TestableDir root, List<String> elements) {
        TestableDir sub = root;
        for (String pathElement : elements) {
            sub = sub.getOrCreateSubDir(pathElement);
        }

        return sub;
    }
}