package ab;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import ab.util.Converter;
import org.junit.Test;


public class ConverterTest {

    @Test
    public void timeStringFromSeconds() {
        assertThat(Converter.timeStringFromSeconds(131), is("2:11"));
    }

}