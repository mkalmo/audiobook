package ab.dbus2;

import org.freedesktop.dbus.DBusConnection;

public class Client2 {

   public static class two_part_test_object implements TwoPartObject {

       public boolean isRemote() {
            return false;
        }

        public String getName() {
            System.out.println("client name");
            return toString();
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("get conn");
        DBusConnection conn = DBusConnection.getConnection(DBusConnection.SESSION);
        System.out.println("get remote");
        TwoPartInterface remote = conn.getRemoteObject(
                        "ee.myServer", "/",
                        TwoPartInterface.class);
        System.out.println("get object");
        TwoPartObject o = remote.getNew();
        System.out.println("get name");
        System.out.println(o.getName());
        two_part_test_object tpto = new two_part_test_object();
        conn.exportObject("/TestObject", tpto);
        conn.sendSignal(new TwoPartInterface.TwoPartSignal("/FromObject", tpto));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException Ie) {
        }
        conn.disconnect();
    }
}
