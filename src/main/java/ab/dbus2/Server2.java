package ab.dbus2;

import org.freedesktop.dbus.DBusConnection;
import org.freedesktop.dbus.DBusSigHandler;

public class Server2 implements TwoPartInterface,
        DBusSigHandler<TwoPartInterface.TwoPartSignal> {

   public class two_part_test_object implements TwoPartObject {
        public boolean isRemote() {
            return false;
        }

        public String getName() {
            System.out.println("give name");
            return toString();
        }
    }

    private DBusConnection conn;

    public Server2(DBusConnection conn) {
        this.conn = conn;
    }

    public boolean isRemote() {
        return false;
    }

    public TwoPartObject getNew() {
        TwoPartObject o = new two_part_test_object();
        System.out.println("export new");

        try {
            conn.exportObject("/12345", o);
        } catch (Exception e) {
        }
        System.out.println("give new");
        return o;
    }

    public void handle(TwoPartSignal s) {
        System.out.println("Got: " + s.o);
    }

    public static void main(String[] args) throws Exception {
        DBusConnection conn = DBusConnection.getConnection(DBusConnection.SESSION);
        conn.requestBusName("ee.myServer");

        Server2 server = new Server2(conn);
        conn.exportObject("/", server);
        conn.addSigHandler(TwoPartSignal.class, server);

        while (true) try {
            Thread.sleep(10000);
        } catch (InterruptedException Ie) {

        }
    }
}

