package ab.fx;

import java.io.File;
import java.util.List;

import ab.common.DirChangeWatcher;
import ab.common.PlayerModel;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;
import ab.playlist.PlaylistLoader;
import ab.playlist.PlaylistSaver;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        FXMLLoader loader = new FXMLLoader();

        loader.setController(buildController(getAudioDir()));

        Parent root = loader.load(
                getClass().getClassLoader()
                .getResourceAsStream("playerUi.fxml"));

        Scene scene = new Scene(root);

        scene.getStylesheets().add
            (getClass().getClassLoader()
                    .getResource("style.css").toExternalForm());

        stage.setScene(scene);
        stage.show();
    }

    private File getAudioDir() {

        List<String> args = getParameters().getUnnamed();

        if (args.isEmpty()) {
            exit("Provide audiobook directory as an argument");
        }

        File audioDir = new File(args.stream().findFirst().get());

        if (!audioDir.exists()) {
            exit(audioDir + " does not exist");
        } else if (!audioDir.isDirectory()) {
            exit(audioDir + " is not dir");
        } else if (!audioDir.canWrite()) {
            exit(audioDir + " is not writable");
        }

        return audioDir;
    }

    public static void exit(String message) {
        System.out.println(message);

        new ErrorDialog(message, e -> { System.exit(0); });
    }

    private PlayerUiController buildController(File audioDir) {
        DirChangeWatcher watcher = new DirChangeWatcher(audioDir);

        PlayerModel player = new PlayerModel(
                new PlaylistLoader(audioDir),
                new FxMediaPlayer(),
                new PlaylistSaver(audioDir));

        return new PlayerUiController(player, watcher);
    }

}
