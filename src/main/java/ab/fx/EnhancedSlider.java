package ab.fx;

import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;

public class EnhancedSlider extends Slider {

    private final long delay = 100;
    private long lastTimeMousePressed = 0;

    public EnhancedSlider() {
        addEventFilter(MouseEvent.MOUSE_PRESSED, e -> {
            lastTimeMousePressed = System.currentTimeMillis();
        });
    }

    public boolean wasMousePressed() {
        return (System.currentTimeMillis() - lastTimeMousePressed) <= delay;
    }

}
