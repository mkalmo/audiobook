package ab.fx;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Consumer;


import ab.common.DirChangeWatcher;
import ab.common.PlayerModel;
import ab.dbus.Player;
import ab.util.Formatter;
import javafx.animation.*;
import javafx.beans.value.*;
import javafx.collections.*;
import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import org.freedesktop.dbus.DBusConnection;
import org.freedesktop.dbus.exceptions.DBusException;

public class PlayerUiController implements Initializable {

    @FXML
    private Button btnPlay;

    @FXML
    private Button btnTest;

    @FXML
    private Button btnNext;

    @FXML
    private Button btnStop;

    @FXML
    private Label lblTime;

    @FXML
    private Label lblFile;

    @FXML
    private EnhancedSlider slider;

    @FXML
    private ListView<String> listViewFiles;

    @FXML
    private ListView<String> listViewDirs;

    private final ObservableList<String> listFiles = FXCollections
            .observableArrayList();

    private final ObservableList<String> listDirs = FXCollections
            .observableArrayList();

    private PlayerModel player;

    private DirChangeWatcher dirChangeWatcher;

    public PlayerUiController(PlayerModel player, DirChangeWatcher dirChangeWatcher) {
        this.player = player;
        this.dirChangeWatcher = dirChangeWatcher;

        startDbusServer();
    }

    private void startDbusServer() {
        try {
            DBusConnection conn = DBusConnection.getConnection(DBusConnection.SESSION);

            conn.requestBusName("org.mpris.MediaPlayer2.Cozy");

            conn.exportObject("/org/mpris/MediaPlayer2", new Player() {

                @Override
                public boolean isRemote() {
                    return false;
                }

                @Override
                public void PlayPause() {
                    playAction(null);
                }

                @Override
                public void Previous() {
                    player.rewind(15);
                }
            });
        } catch (DBusException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    public void playAction(ActionEvent action) {
        if (player.isPlaying()) {
            System.out.println("pause");
            player.pause();
        } else {
            System.out.println("play");
            player.play();
        }
    }

    private ChangeListener<Number> getSliderChangeListener() {
        return (o, oldValue, newValue) -> {
            if (slider.isValueChanging() || slider.wasMousePressed()) {
                player.seek((int) newValue.floatValue());
            }
        };
    }

    @FXML
    public void nextAction(ActionEvent action) {
        System.out.println("Next pressed");

        player.next();

        updateDisplay();
    }

    @FXML
    public void stopAction(ActionEvent action) {
        System.out.println("Stop pressed");

        player.stop();
    }

    @FXML
    public void pauseAction(ActionEvent action) {
        System.out.println("Pause pressed");

        player.pause();
    }

    @FXML
    public void testAction(ActionEvent action) {
        System.out.println("Test Button Pushed");

        player.savePlaylist();
    }

    private void updateListViews() {

        listFiles.setAll(player.getPlaylist().getFileNames());
        listDirs.setAll(player.getPlaylist().getDirNames());

        listViewFiles.getSelectionModel().select(
                player.getPlaylist().getCurrentFileName());

        listViewDirs.getSelectionModel().select(
                player.getPlaylist().getCurrentDir());
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        listViewFiles.setItems(listFiles);
        listViewDirs.setItems(listDirs);

        slider.valueProperty().addListener(getSliderChangeListener());

        listViewDirs.setOnMouseClicked(getListViewDirsSelectAction());
        listViewFiles.setOnMouseClicked(getListViewFilesSelectAction());

        startTimer(1, e -> {
            updateDisplay();
            updateListViews();
            player.updateCurrentTime();
        });

        startTimer(5, e -> {
            if (dirChangeWatcher.changeHappened()) {
                System.out.println("reload");
                player.reloadPlaylist();
            }
            if (player.isPlaying()) {
                player.savePlaylist();
            }
        });
    }

    private EventHandler<? super MouseEvent> getListViewDirsSelectAction() {
        return e -> {
            dblClickOnListItemCommon(e, listViewDirs, dir -> {
                player.getPlaylist().selectDir(dir);
            });
        };
    }

    private EventHandler<? super MouseEvent> getListViewFilesSelectAction() {
        return e -> {
            dblClickOnListItemCommon(e, listViewFiles, file -> {
                player.getPlaylist().selectFile(file);
            });
        };
    }

    private void dblClickOnListItemCommon(MouseEvent e,
            ListView<String> list, Consumer<String>  consumer) {
        if (e.getClickCount() < 2) {
            return;
        }

        Optional<String> first = list.getSelectionModel()
                .getSelectedItems().stream().findFirst();

        if (first.isPresent()) {
            consumer.accept(first.get());
            player.stop();
            player.play();
        }
    }

    private void startTimer(Integer interval, EventHandler<ActionEvent> onFinished) {
        Timeline timer = new Timeline(new KeyFrame(
                Duration.seconds(interval), onFinished));
        timer.setCycleCount(Timeline.INDEFINITE);
        timer.play();
    }

    private void updateDisplay() {

        String text = Formatter.getDispalyString(
                player.getCurrentTime(), player.getTotalDuration(),
                player.getPlaylist().getCurrentFileName());
        lblTime.setText(text);
        slider.setMax(player.getTotalDuration());

        slider.setValue(player.getCurrentTime());

        String playButtonLabel = player.isPlaying() ? "Pause" : "Play";
        btnPlay.setText(playButtonLabel);
    }
}
