package ab.fx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ErrorDialog {

    Button button = new Button("Ok");

    public ErrorDialog(String message, EventHandler<ActionEvent> handler) {
        button.setOnAction(handler);

        VBox vBox = new VBox();
        vBox.getChildren().addAll(new Text(message), button);
        vBox.setAlignment(Pos.CENTER);
        vBox.setPadding(new Insets(25));

        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.setScene(new Scene(vBox));
        dialogStage.showAndWait();
    }
}
