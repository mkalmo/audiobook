package ab.fx;

import java.io.File;
import java.net.MalformedURLException;

import javafx.scene.media.MediaPlayer;
import javafx.scene.media.Media;
import javafx.util.Duration;

public class FxMediaPlayer implements ab.common.MediaPlayer {

    private MediaPlayer mediaPlayer;
    private File file;

    @Override
    public void load(File file, Runnable onReady) {

        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.dispose();
        }

        this.file = file;
        mediaPlayer = new MediaPlayer(new Media(toUrl(file)));
        mediaPlayer.setOnReady(onReady);
    }

    @Override
    public void play() {
        mediaPlayer.play();
    }

    @Override
    public File getFile() {
        return file;
    }

    @Override
    public void seek(Integer position) {
        mediaPlayer.seek(new Duration(position * 1000));
    }

    @Override
    public void stop() {
        if (mediaPlayer == null) {
            return;
        }

        mediaPlayer.stop();
    }

    @Override
    public void setOnEndOfMedia(Runnable runnable) {
        mediaPlayer.setOnEndOfMedia(runnable);
    }

    @Override
    public Integer getTotalDuration() {
        return (int) mediaPlayer.getTotalDuration().toSeconds();
    }

    @Override
    public Integer getCurrentTime() {
        return (int) mediaPlayer.getCurrentTime().toSeconds();
    }

    @Override
    public void pause() {
        mediaPlayer.pause();
    }

    @Override
    public boolean isReady() {
        if (mediaPlayer == null) {
            return false;
        }

        return mediaPlayer.getStatus() != MediaPlayer.Status.UNKNOWN;
    }

    @Override
    public boolean isPlaying() {
        if (mediaPlayer == null) {
            return false;
        }

        return mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING;
    }

    private String toUrl(File file) {
        try {
            return file.toURI().toURL().toExternalForm();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

}
