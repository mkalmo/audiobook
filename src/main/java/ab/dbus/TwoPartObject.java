package ab.dbus;

import org.freedesktop.dbus.DBusInterface;

public interface TwoPartObject extends DBusInterface {
   String getName();
}
