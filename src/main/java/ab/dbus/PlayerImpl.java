package ab.dbus;

public class PlayerImpl implements Player {

    @Override
    public void PlayPause() {
        System.out.println("play");
    }

    @Override
    public void Previous() {
        System.out.println("previous");
    }

    @Override
    public boolean isRemote() {
        return false;
    }
}
