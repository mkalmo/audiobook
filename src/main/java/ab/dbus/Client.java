package ab.dbus;

import org.freedesktop.dbus.DBusConnection;

public class Client {

   public static class TestObject implements TwoPartObject {

      public boolean isRemote() {
         return false;
      }

      public String getName() {
         System.out.println("client name");
         return toString(); 
      }
   }

   public static void main(String[] args) throws Exception {
      System.out.println("get conn");
      DBusConnection conn = DBusConnection.getConnection(DBusConnection.SESSION);

      System.out.println("get remote");
      Player remote = conn.getRemoteObject(
              "org.freedesktop.dbus.test.two_part_server", "/",
              Player.class);

      System.out.println("get object");
//      TwoPartObject o = remote.getNew();
      System.out.println("get name");
//      System.out.println(o.getName());

      TestObject tpto = new TestObject();
      conn.exportObject("/TestObject", tpto);

//      conn.sendSignal(new TwoPartInterface.TwoPartSignal("/FromObject", tpto));

      try {
         Thread.sleep(1000);
      } catch (InterruptedException Ie) {
         System.out.println("interrupted..");
      }

      conn.disconnect();
   }
}
