package ab.dbus;

import org.freedesktop.dbus.DBusConnection;

public class Server {

   public static void main(String[] args) throws Exception {

      DBusConnection conn = DBusConnection.getConnection(DBusConnection.SESSION);

      conn.requestBusName("org.mpris.MediaPlayer2.Cozy");

      conn.exportObject("/org/mpris/MediaPlayer2", new PlayerImpl());

      while (true) {
         try {
            Thread.sleep(1000);
         } catch (InterruptedException Ie) {
         }
      }
   }
}

