package ab.dbus;

import org.freedesktop.dbus.DBusInterface;

public interface Player extends DBusInterface {

   void PlayPause();

   void Previous();

}
