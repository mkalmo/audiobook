package ab.playlist;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import ab.util.Mp3Filter;

public class DirEntry {

    protected File currentFile;

    private File dir;
    private Iterator<File> fileIterator;
    private Map<String, Integer> currentTimeMap = new HashMap<>();

    public DirEntry() {}

    public DirEntry(File dir) {
        this.dir = dir;
        fileIterator = getFileList(dir).iterator();
        nextFile();
    }

    public void nextFile() {
        if (fileIterator.hasNext()) {
            currentFile = fileIterator.next();
        } else {
            currentFile = null;
        }
    }

    public void setCurrentTime(Integer currentTime) {
        currentTimeMap.put(currentFile.getName(), currentTime);
    }

    public Integer getCurrentTime() {
        if (!currentTimeMap.containsKey(currentFile.getName())) {
            return 0;
        }

        return currentTimeMap.get(currentFile.getName());
    }

    public boolean hasCurrentFile() {
        return currentFile != null;
    }

    public Optional<File> getCurrentFileEntry() {
        if (hasCurrentFile()) {
            return Optional.of(currentFile);
        } else {
            return Optional.empty();
        }
    }

    public String getName() {
        return dir.getName();
    }

    public void selectFile(String fileName) {
        fileIterator = getFileList(dir).iterator();
        do {
            nextFile();
            if (currentFile == null) {
                break;
            }

        } while (!currentFile.getName().equals(fileName));
    }

    public List<File> getFileList() {
        return getFileList(dir);
    }

    private List<File> getFileList(File dir) {
        ArrayList<File> fileEntries = new ArrayList<>();
        for (File file : dir.listFiles(new Mp3Filter())) {
            fileEntries.add(file);
        }

        Collections.sort(fileEntries,
                (f1, f2) -> f1.getName().compareTo(f2.getName()));

        return fileEntries;
    }
}
