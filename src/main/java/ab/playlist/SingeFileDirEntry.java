package ab.playlist;

import java.io.File;
import java.util.*;

public class SingeFileDirEntry extends DirEntry {

    public SingeFileDirEntry(File singeFile) {
        currentFile = singeFile;
    }

    @Override
    public String getName() {
        return currentFile.getName();
    }

    @Override
    public Optional<File> getCurrentFileEntry() {
        return Optional.of(currentFile);
    }

    @Override
    public void selectFile(String fileName) {}

    @Override
    public void nextFile() {}

    @Override
    public List<File> getFileList() {
        return Arrays.asList(currentFile);
    }
}
