package ab.playlist;

import java.io.File;

import ab.util.Mp3Filter;
import ab.util.Util;
import ab.common.Constants;

public class PlaylistLoader {

    private File audioDir;

    public PlaylistLoader(File audioDir) {
        this.audioDir = audioDir;
    }

    public Playlist load() {
         Playlist playlist = new Playlist();

         loadFromAudioDir(playlist);

         playlist.applyState(loadPlaylistFile());

         return playlist;
     }

     protected String loadPlaylistFile() {
         File savedPlaylistInfo = new File(audioDir, Constants.PLAYLIST_FILE);
         return savedPlaylistInfo.exists()
                 ? Util.readFile(savedPlaylistInfo)
                 : "";
     }

     private Playlist loadFromAudioDir(Playlist playlist) {
        for (File subdir : audioDir.listFiles()) {
            if (!subdir.isDirectory()) {
                continue;
            }
            playlist.addDir(subdir);
        }
        for (File mp3 : audioDir.listFiles(new Mp3Filter())) {
            playlist.addFile(mp3);
        }

        return playlist;
    }
}