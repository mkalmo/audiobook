package ab.playlist;

import java.io.File;

public class FileEntry implements Comparable<FileEntry> {

    public final File file;
    public Integer currentTime;

    public FileEntry(File file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return file.getName() + " " + System.identityHashCode(this);
    }

    @Override
    public int compareTo(FileEntry fileEntry) {
        return file.getName().compareTo(fileEntry.file.getName());
    }

    public String getName() {
        return file.getName();
    }

    public void setCurrentTime(Integer position) {
        this.currentTime = position;
    }

    public Integer getCurrentTime() {
        return currentTime != null ? currentTime : 0;
    }

    public File getFile() {
        return file;
    }
}
