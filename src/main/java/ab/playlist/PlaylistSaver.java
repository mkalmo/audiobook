package ab.playlist;

import java.io.File;
import java.nio.file.*;

import ab.common.Constants;
import ab.util.Util;

public class PlaylistSaver {

    private File audioDir;

    public PlaylistSaver(File audioDir) {
        this.audioDir = audioDir;
    }

    public void savePlaylist(String content) {
        File tempFile = null;
        try {
            tempFile = File.createTempFile("playlist-", ".tmp");

            Util.writeFile(tempFile, content);

            Files.move(
                    tempFile.toPath(),
                    new File(audioDir, Constants.PLAYLIST_FILE).toPath(),
                    StandardCopyOption.REPLACE_EXISTING);

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            tempFile.delete();
        }
    }


}
