package ab.playlist;

import java.io.File;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.Supplier;

import ab.common.PlaylistPublic;
import static java.util.stream.Collectors.*;

public class Playlist implements PlaylistPublic {

    private List<DirEntry> entries = new ArrayList<DirEntry>();
    private DirEntry currentDir;

    @Override
    public List<String> getDirNames() {
        return entries.stream().map(
                e -> e.getName()).collect(toList());
    }

    @Override
    public List<String> getFileNames() {
        if (!getCurrentDirEntry().isPresent()) {
            return Arrays.asList();
        }

        return getCurrentDirEntry().get().getFileList().stream().map(
                e -> e.getName()).collect(toList());
    }

    @Override
    public void next() {
        if (getCurrentDirEntry().isPresent()) {
            getCurrentDirEntry().get().nextFile();
        }
    }

    @Override
    public void selectFile(String fileName) {
        if (getCurrentDirEntry().isPresent()) {
            getCurrentDirEntry().get().selectFile(fileName);
        }
    }

    @Override
    public void selectDir(String dirAsString) {
        Optional<DirEntry> matching = entries.stream()
                .filter(e -> e.getName().equals(dirAsString))
                .findFirst();

        if (matching.isPresent()) {
            currentDir = matching.get();
            return;
        } else {
            throw new IllegalArgumentException("no such dir: " + dirAsString);
        }
    }

    @Override
    public String getCurrentFileName() {
        if (hasCurrentFile()) {
            return getCurrentDirEntry().get().
                    getCurrentFileEntry().get().getName();
        } else {
            return null;
        }
    }

    public Optional<File> getCurrentFile() {
        if (getCurrentDirEntry().isPresent()) {
            return getCurrentDirEntry().get().getCurrentFileEntry();
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getCurrentDir() {
        return getCurrentDirEntry().isPresent()
                ? getCurrentDirEntry().get().getName()
                : null;
    }

    public void setCurrentTime(Integer time) {
        if (getCurrentDirEntry().isPresent()
                && getCurrentDirEntry().get().hasCurrentFile()) {
            getCurrentDirEntry().get().setCurrentTime(time);
        }
    }

    public Integer getCurrentTime() {
        if (getCurrentDirEntry().isPresent()
                && getCurrentDirEntry().get().hasCurrentFile()) {
            return getCurrentDirEntry().get().getCurrentTime();
        } else {
            return 0;
        }
    }

    public void addDir(File dir) {
        addDirSub(dir.getName(), () -> new DirEntry(dir));
    }

    public void addFile(File file) {
        addDirSub(file.getName(), () -> new SingeFileDirEntry(file));
    }

    private void addDirSub(String name, Supplier<DirEntry> supplier) {
        Optional<DirEntry> existing = entries.stream()
                .filter(e -> e.getName().equals(name))
                .findFirst();

        if (existing.isPresent()) {
            currentDir = existing.get();
        } else {
            currentDir = supplier.get();
            entries.add(currentDir);
        }
    }

    public boolean hasCurrentFile() {
        return getCurrentDirEntry().isPresent()
                && getCurrentDirEntry().get()
                        .getCurrentFileEntry().isPresent();
    }

    private Optional<DirEntry> getCurrentDirEntry() {
        if (currentDir != null) {
            return Optional.of(currentDir);
        } else {
            return Optional.empty();
        }
    }

    public String serializeState() {
        String retval = "";
        for (DirEntry e : entries) {

            if (!e.hasCurrentFile()) {
                continue;
            }

            String isActive = currentDir == e ? "1" : "0";
            retval += MessageFormat.format("{0}|{1}|{2}|{3}\n",
                    isActive, unixSeparators(e.getName()),
                    e.getCurrentFileEntry().get().getName(),
                    e.getCurrentTime());
        }

        return retval;
    }

    private String unixSeparators(String path) {
        return path.replaceAll("\\\\", "/");
    }

    public void applyState(String state) {
        Scanner lineScanner = new Scanner(state);
        lineScanner.useDelimiter("\n");
        String activeDir = null;
        while (lineScanner.hasNext()) {

            PlaylistRow row = parseRow(lineScanner.next());

            Optional<DirEntry> matchingDir = entries.stream()
                .filter(e -> e.getName().equals(row.dir))
                .findFirst();

            if (!matchingDir.isPresent()) {
                continue;
            }

            if ("1".equals(row.isActive)) {
                activeDir = row.dir;
            }

            matchingDir.get().setCurrentTime(row.position);
            matchingDir.get().selectFile(row.fileName);

//            Optional<File> matchingFile = matchingDir.get()
//                    .getFileList().stream()
//                    .filter(f -> f.getName().equals(row.fileName))
//                    .findFirst();
//
//            if (matchingFile.isPresent()) {
//
//                matchingFile.get().setCurrentTime(row.position);
//
//                matchingDir.get().selectFile(row.fileName);
//            }

        }

        if (activeDir != null) {
            selectDir(activeDir);
        }

        lineScanner.close();
    }

    private PlaylistRow parseRow(String rowAsString) {
        PlaylistRow row = new PlaylistRow();

        Scanner rowScanner = new Scanner(rowAsString);
        rowScanner.useDelimiter("\\|");

        row.isActive = rowScanner.next();
        row.dir = rowScanner.next();
        row.fileName = rowScanner.next();
        row.position = rowScanner.nextInt();

        rowScanner.close();
        return row;
    }

    private static class PlaylistRow {
        String isActive;
        String dir;
        String fileName;
        Integer position;
    }

    protected File createFile(String dir) {
        return new File(dir);
    }
}