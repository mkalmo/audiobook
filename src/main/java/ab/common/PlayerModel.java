package ab.common;

import java.io.File;
import java.util.List;

import ab.playlist.Playlist;
import ab.playlist.PlaylistLoader;
import ab.playlist.PlaylistSaver;

public class PlayerModel {

    private Playlist playlist;
    private MediaPlayer mediaPlayer;
    private PlaylistSaver playlistSaver;
    private PlaylistLoader playlistLoader;

    public PlayerModel(PlaylistLoader playlistLoader, MediaPlayer mediaPlayer, PlaylistSaver playlistSaver) {
        this.playlistLoader = playlistLoader;
        this.mediaPlayer = mediaPlayer;
        this.playlistSaver = playlistSaver;
        this.playlist = playlistLoader.load();
    }

    public void reloadPlaylist(PlaylistLoader playlistLoader) {
        boolean wasPlaying = mediaPlayer.isPlaying();
        if (wasPlaying) {
            stop();
        }
        this.playlist = playlistLoader.load();
        if (wasPlaying) {
            play();
        }
    }

    public void reloadPlaylist() {
        reloadPlaylist(playlistLoader);
    }

    public PlaylistPublic getPlaylist() {
        return playlist;
    }

    public void play() {
        if (!playlist.hasCurrentFile()) {
            return;
        }

        if (mediaPlayer.isReady() && fileIsNotChanged()) {
            mediaPlayer.play();
        } else {
            File file = playlist.getCurrentFile().get();
            mediaPlayer.load(file, () -> {
                mediaPlayer.seek(playlist.getCurrentTime());
                mediaPlayer.play();
            });
            mediaPlayer.setOnEndOfMedia(
                    () -> {
                        playlist.next();
                        play();
                    });
        }
    }

    private boolean fileIsNotChanged() {
        if (playlist.hasCurrentFile()) {
            return playlist.getCurrentFile().get()
                .getAbsoluteFile().equals(mediaPlayer.getFile().getAbsoluteFile());
        } else {
            return false;
        }
    }

    public void stop() {
        mediaPlayer.stop();
    }

    public void next() {
        stop();

        playlist.next();

        play();
    }

    public List<String> getFileList() {
        return playlist.getFileNames();
    }

    public List<String> getDirList() {
        return playlist.getDirNames();
    }

    public void seek(Integer pos) {
        mediaPlayer.seek(pos);
    }

    public void rewind(int seconds) {
        mediaPlayer.seek(mediaPlayer.getCurrentTime() - seconds);
    }

    public Integer getTotalDuration() {
        return mediaPlayer.isReady() ? mediaPlayer.getTotalDuration() : 0;
    }

    public Integer getCurrentTime() {
        return mediaPlayer.isReady() ? mediaPlayer.getCurrentTime() : 0;
    }

    public void pause() {
        mediaPlayer.pause();
    }

    public void updateCurrentTime() {
        if (playlist.hasCurrentFile() && mediaPlayer.isPlaying()) {
            playlist.setCurrentTime(mediaPlayer.getCurrentTime());
        }
    }

    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    public void savePlaylist() {
        playlistSaver.savePlaylist(playlist.serializeState());
    }
}
