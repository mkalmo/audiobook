package ab.common;

import java.util.List;

public interface PlaylistPublic {

    void next();

    void selectFile(String fileName);

    void selectDir(String dirAsString);

    String getCurrentFileName();

    String getCurrentDir();

    List<String> getDirNames();

    List<String> getFileNames();
}