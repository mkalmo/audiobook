package ab.common;

import java.io.*;
import java.nio.file.*;
import java.util.Optional;

import static java.nio.file.StandardWatchEventKinds.*;

public class DirChangeWatcher {

    private WatchService watcher;

    public DirChangeWatcher(File dirTowatch) {
        Path path = dirTowatch.toPath();

        try {
            watcher = path.getFileSystem().newWatchService();
            path.register(watcher, ENTRY_CREATE, ENTRY_DELETE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean changeHappened() {
        WatchKey watckKey = watcher.poll();
        if (watckKey == null) {
            return false;
        }

        Optional<WatchEvent<?>> event = watckKey.pollEvents()
                .stream().filter(e -> {
                    String fileName = e.context().toString();
                    return !Constants.PLAYLIST_FILE.equals(fileName);
        }).findFirst();

        watckKey.reset();

        return event.isPresent();
    }
}
