package ab.common;

import java.io.File;

public interface MediaPlayer {

	void load(File file, Runnable onReady);

	void play();

	File getFile();

	void seek(Integer position);

	void stop();

	void setOnEndOfMedia(Runnable runnable);

	Integer getTotalDuration();

	Integer getCurrentTime();

	void pause();

	boolean isReady();

	boolean isPlaying();

}