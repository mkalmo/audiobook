package ab.util;

import java.text.MessageFormat;

public class Converter {

    public static String timeStringFromSeconds(Integer seconds) {
        int minutes = seconds / 60;
        int remaining = seconds - minutes * 60;

        return MessageFormat.format("{0}:{1}", minutes, remaining);
    }

}