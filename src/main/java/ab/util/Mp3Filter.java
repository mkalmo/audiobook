package ab.util;

import java.io.File;
import java.io.FilenameFilter;

public class Mp3Filter implements FilenameFilter {

    @Override
    public boolean accept(File dir, String name) {
        return name.matches(".*\\.mp3");
    }

}
