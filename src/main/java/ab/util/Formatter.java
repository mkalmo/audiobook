package ab.util;

import java.text.MessageFormat;

public class Formatter {

    public static String getTimeString(Integer current, Integer total) {
        return MessageFormat.format("{0} of {1}",
                Converter.timeStringFromSeconds(current),
                Converter.timeStringFromSeconds(total));
    }

    public static String getDispalyString(
            Integer current, Integer total, String fileName) {

        if (fileName == null) {
            return "Not loaded";
        }

        return MessageFormat.format("{0} {1}",
                getTimeString(current, total), fileName);
    }

}
