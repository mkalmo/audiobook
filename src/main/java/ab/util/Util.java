package ab.util;

import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.Charset;

public class Util {

    private static final String ENCODING_UTF_8 = "UTF-8";

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, ENCODING_UTF_8);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String readFile(File f) {
        return readFileSub(f, Charset.forName("UTF-8"));
    }

    public static void writeFile(File f, String content) {
        try {
            OutputStream out = new FileOutputStream(f);
            Writer w = new BufferedWriter(
                    new OutputStreamWriter(out, Charset.forName("UTF-8")));
            w.write(content);
            w.flush();
            w.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String readFileSub(File f, Charset charset) {
        try {
            return readFromStream(new FileInputStream(f.getPath()), charset);
        } catch (IOException e) {
            throw new RuntimeException("file read error", e);
        }
    }

    public static String readFromStream(InputStream is, Charset charset) {

        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(is, charset));
        } catch (Exception e) {
            throw new RuntimeException("can't read file: " + e);
        }

        StringBuilder sb = new StringBuilder();
        String line = null;
        boolean firstLine = true;
        try {
            while ((line = reader.readLine()) != null) {
                if (firstLine == false) {
                    sb.append("\n");
                }
                firstLine = false;

                sb.append(line);
            }
        } catch (IOException e) {
            throw new RuntimeException("file read error", e);
        } finally {
            close(reader);
        }

        return sb.toString();
    }

    private static void close(Closeable c) {
        try {
            c.close();
        } catch (IOException ignore) {}

    }
}
