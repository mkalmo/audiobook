# SCREENSHOT #

https://bitbucket.org/mkalmo/audiobook/downloads/audioplayer.png

# BINARY #

https://bitbucket.org/mkalmo/audiobook/downloads/jaudiobook.jar

# RUN #

java -jar <path>/jaudiobook.jar <audiobooks dir>

eg.

java -jar jaudiobook.jar ~/Audiobooks

# BUILD #

mvn package

builds executable jar (target/jaudiobook.jar)